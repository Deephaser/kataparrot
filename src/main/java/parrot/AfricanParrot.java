package parrot;

public class AfricanParrot extends Parrot {
    protected int numberOfCoconuts;
    public AfricanParrot(int numberOfCoconuts) {
        this.numberOfCoconuts = numberOfCoconuts;
    }
    public double getSpeed()
    {
        return Math.max(0, this.getBaseSpeed() - this.getLoadFactor() * numberOfCoconuts);
    }
    protected double getLoadFactor() {
        return 9.0;
    }
}
